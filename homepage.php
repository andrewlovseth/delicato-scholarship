<?php

/*
    Template Name: Home
*/

get_header(); ?>

    <?php get_template_part('template-parts/home/hero'); ?>

    <?php get_template_part('template-parts/home/quote'); ?>

    <?php get_template_part('template-parts/home/video'); ?>

    <?php get_template_part('template-parts/home/overview'); ?>

    <?php get_template_part('template-parts/home/benefits'); ?>

    <?php get_template_part('template-parts/home/requirements'); ?>

    <?php get_template_part('template-parts/home/apply'); ?>

    <?php get_template_part('template-parts/home/faqs'); ?>

    <?php get_template_part('template-parts/home/application'); ?>

<?php get_footer(); ?>