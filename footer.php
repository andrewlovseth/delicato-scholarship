	</main> <!-- .site-content -->

	<footer class="site-footer grid">
		<?php get_template_part('template-parts/footer/logo'); ?>

		<?php get_template_part('template-parts/footer/navigation'); ?>

		<?php get_template_part('template-parts/footer/copyright'); ?>
	</footer>

<?php wp_footer(); ?>

</div> <!-- .site -->

</body>
</html>